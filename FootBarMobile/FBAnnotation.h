//
//  FBAnnotation.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FBAnnotation : NSObject <MKAnnotation> {

}
@property (nonatomic) NSInteger idMatch;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id)initWithCoordinate:(CLLocationCoordinate2D) c;
-(id)initWithCoordinate:(CLLocationCoordinate2D)c withName:(NSString*)name withAddress:(NSString*)address withIdMatch:(NSInteger)i;
- (CLLocationCoordinate2D) coordinate;
@end
