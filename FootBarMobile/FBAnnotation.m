//
//  FBAnnotation.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import "FBAnnotation.h"

@implementation FBAnnotation

@synthesize title, subtitle, coordinate, idMatch;

- (id)initWithCoordinate:(CLLocationCoordinate2D) c {
    coordinate = c;
    title = @"Title";
    subtitle = @"Subtitle";
    return self;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c withName:(NSString*)name withAddress:(NSString*)address withIdMatch:(NSInteger)i{
    self = [super init];
    if (self) {
        coordinate = c;
        title = name;
        subtitle = address;
        idMatch = i;
    }
    return self;
}

- (CLLocationCoordinate2D) coordinate {
    return coordinate;
}

- (NSString *) title {
    return title;
}

- (NSString *) subtitle {
    return subtitle;
}

- (NSInteger) idMatch {
    return idMatch;
}
@end
