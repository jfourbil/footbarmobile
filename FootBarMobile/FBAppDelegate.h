//
//  FBAppDelegate.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBDataManager.h"

@interface FBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) FBDataManager *manager;

@end
