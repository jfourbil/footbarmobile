//
//  FBConstants.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBConstants : NSObject

// Models constants
FOUNDATION_EXPORT NSString *const MESSAGES;
FOUNDATION_EXPORT NSString *const LATITUDE;
FOUNDATION_EXPORT NSString *const LONGITUDE;
FOUNDATION_EXPORT NSString *const TEAM_A;
FOUNDATION_EXPORT NSString *const TEAM_B;
FOUNDATION_EXPORT NSString *const ID_MATCH;

// URLs constants
FOUNDATION_EXPORT NSString *const REMOTE_URL;
FOUNDATION_EXPORT NSString *const REMOTE_MESSAGES;
FOUNDATION_EXPORT NSString *const REMOTE_MATCHES;

// Statuses constants
FOUNDATION_EXPORT NSString *const CONNECTION_SUCCEED;
FOUNDATION_EXPORT NSString *const CONNECTION_FAILED;

// Parameters
FOUNDATION_EXPORT NSInteger GAME_FOLLOWED;

@end
