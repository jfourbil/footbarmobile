//
//  FBConstants.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import "FBConstants.h"

@implementation FBConstants

// Models constants
NSString *const MESSAGES = @"messages";
NSString *const LATITUDE = @"lat";
NSString *const LONGITUDE = @"lon";
NSString *const TEAM_A = @"teamA";
NSString *const TEAM_B = @"teamB";
NSString *const ID_MATCH = @"id";

// URLs constants
NSString *const REMOTE_URL = @"http://137.194.15.209/";
NSString *const REMOTE_MESSAGES = @"messages";
NSString *const REMOTE_MATCHES = @"matches";

// Statuses constants
NSString *const CONNECTION_SUCCEEDED = @"connection succeeded";
NSString *const CONNECTION_FAILED = @"connection failed";

// Parameters
NSInteger GAME_FOLLOWED = 1;
@end