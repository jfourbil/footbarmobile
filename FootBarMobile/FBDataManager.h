//
//  FBDataManager.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBMatch.h"

@interface FBDataManager : NSObject

@property (nonatomic, retain) NSTimer * timer;
@property (nonatomic) BOOL fetched;
@property (nonatomic) NSMutableArray *matches;
@property (nonatomic) NSMutableArray *messages;
@property (nonatomic) NSOperationQueue *queue;
@property (nonatomic) NSMutableArray *voteArray;

- (id) init;
- (NSMutableArray *) makeRequestByFamily:(NSString *)family;
- (NSDictionary *) getMatchById:(int) k;
- (NSMutableArray *) getMessagesById:(int) k;
- (BOOL) postMessageForId:(int)k text:(NSString *)string;
- (void) resetTimer;
- (void) makeAsyncRequestByFamily:(NSString *)family;
- (void) receivedData:(NSData *)data withFamily:(NSString *)family;
- (BOOL) voteInDirection: (NSString *) direction forMessage: (NSInteger) id;

@end
