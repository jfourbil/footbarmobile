//
//  FBDataManager.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import "FBDataManager.h"
#import "FBConstants.h"

@implementation FBDataManager
@synthesize matches, messages, fetched, timer, voteArray;
@synthesize queue;
- (id) init {
    self = [super init];
    if(!self)
        return self;
    fetched = FALSE;
    queue = [[NSOperationQueue alloc] init];
    [self makeAsyncRequestByFamily:REMOTE_MESSAGES];
    [self makeAsyncRequestByFamily:REMOTE_MATCHES];
    // initiate the timer
    timer = [ NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(targetMethod:) userInfo:nil repeats: YES];
    voteArray = [[NSMutableArray alloc] init];
    return self;
}
- (void) targetMethod: (NSTimer *) timer {
    [self makeAsyncRequestByFamily:REMOTE_MESSAGES];
    [self makeAsyncRequestByFamily:REMOTE_MATCHES];
}

-(void) resetTimer {
    messages = [self makeRequestByFamily:REMOTE_MESSAGES];
//        [self makeAsyncRequestByFamily:REMOTE_MESSAGES];
}
// async handlers
- (void) makeAsyncRequestByFamily:(NSString *)family
{
    NSLog(@"start async request %@", family);
    // initialize request with the corresponding url
    // by now the server is only able to process GET requests
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[REMOTE_URL stringByAppendingString:family]]];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) {
             [self receivedData:data withFamily:family];
         } else {
             NSLog(@"error");
         }
     }];
}
- (void) receivedData:(NSData *)data withFamily:(NSString *)family{
    fetched = TRUE;
    NSLog(@"async connection succeeded");
    NSError *jsonParsingError = nil;
    // parse JSON response
    NSMutableArray *raw_data = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
    if([family isEqualToString:REMOTE_MESSAGES]){
        messages = raw_data;
    } else {
        matches = raw_data;
    }
}
// end async handlers
- (NSMutableArray *)makeRequestByFamily:(NSString *)family
{
    // initialize error, urlResponse pointers
    fetched = false;
    NSError        *connectionError = nil;
    NSURLResponse  *urlResponse = nil;
    // initialize request with the corresponding url
    // by now the server is only able to process GET requests
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[REMOTE_URL stringByAppendingString:family]]];
    
    // get response
    NSData *response = [NSURLConnection sendSynchronousRequest: request returningResponse: &urlResponse error: &connectionError];
    
    if (connectionError) {
        NSLog(@"connection failed");
        return nil;
    } else {
        fetched = TRUE;
        //NSLog(@"connection succeeded");
        NSError *jsonParsingError = nil;
        // parse JSON response
        NSMutableArray *raw_data = [NSJSONSerialization JSONObjectWithData:response options:0 error:&jsonParsingError];
        //NSLog(@"Array : %@", raw_data);
        return raw_data;
    }
}

- (NSDictionary *) getMatchById:(int)k {
    return [matches objectAtIndex:(k-1)];
}
- (NSMutableArray *) getMessagesById:(int)k {
    
    // Prepare table for messages.
    NSMutableArray *messageTable = [[NSMutableArray alloc]init];

    // Loop in the messages ids of a game.
        // Loop to search for messages where idMatch = k;
        for (NSDictionary *message_container in messages) {
            if ([[message_container objectForKey:@"idMatch"] isEqualToNumber:[NSNumber numberWithInt:(k)]]) {
                [messageTable addObject:message_container];
            }
        
    };
    return messageTable;
}

- (BOOL) postMessageForId:(int)k text:(NSString *)string
{
    NSString *messageWrapped = [NSString stringWithFormat:@"%@%d%@%@", @"idMatch=", k, @"&text=", string];
    NSData *messageBody = [messageWrapped dataUsingEncoding:NSUTF8StringEncoding];
    
    NSHTTPURLResponse  *urlResponse = nil;
    NSError        *connectionError = nil;
    
    // Perform the post request.
    NSMutableURLRequest *post_request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[REMOTE_URL stringByAppendingString:@"message"]]];
    [post_request setHTTPMethod:@"POST"];
    [post_request setHTTPBody:messageBody];
    [NSURLConnection sendSynchronousRequest:post_request returningResponse:&urlResponse error:&connectionError];
    
    return (urlResponse.statusCode <= 300) ? YES : NO;
}

- (BOOL) voteInDirection: (NSString *) direction forMessage: (NSInteger) idMessage
{
    // To prevent from dummy voting.
    if ([voteArray indexOfObject:[NSNumber numberWithInteger:idMessage]] == NSNotFound)
    {
        NSString *familyRequest = [NSString stringWithFormat:@"message/%d/vote/%@", idMessage, direction];
        [self makeAsyncRequestByFamily:familyRequest];
        [voteArray addObject:[NSNumber numberWithInteger:idMessage]];
        return YES;
    }
    else return NO;
}

@end
