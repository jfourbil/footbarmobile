//
//  FBIconAnnotationView.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 21/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FBIconAnnotationView :  MKPinAnnotationView
{
    UIImage *icon;
    UIImageView *iconView;
}
-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;
- (void)setAnnotation:(id)annotation;
- (void)setImage:(UIImage *)image;
@end
