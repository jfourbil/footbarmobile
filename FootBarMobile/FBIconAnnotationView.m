//
//  FBIconAnnotationView.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 21/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import "FBIconAnnotationView.h"
#import "FBAnnotation.h"

@implementation FBIconAnnotationView

- (id)initWithAnnotation:(id)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.animatesDrop=TRUE;
        // Compensate frame a bit so everything's aligned
        [self setCenterOffset:CGPointMake(-9, -3)];
        [self setCalloutOffset:CGPointMake(-2, 3)];
        
        // Add the pin icon
        iconView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 0, 32, 37)];
        [self addSubview:iconView];
    }
    return self;
}
- (void)setAnnotation:(id)annotation {
    [super setAnnotation:annotation];
    icon = [UIImage imageNamed:@"stadium_small.png"];
    [iconView setImage:icon];
}
- (void)setImage:(UIImage *)image {
    [super setImage:[UIImage imageNamed:@"pin_shadow.png"]];
}
@end
