//
//  FBLiveViewController.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBAppDelegate.h"

@interface FBLiveViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *messageList;
    FBAppDelegate *mainAppObject;
}

@property (weak, nonatomic) IBOutlet UITextField *textInput;
@property (weak, nonatomic) IBOutlet UITableView *messageTable;
@property (weak, nonatomic) IBOutlet UILabel *homeTeam;
@property (weak, nonatomic) IBOutlet UILabel *awayTeam;

- (IBAction)sendMessage:(id)sender;
- (void) getMessageList;
- (void) upVote:(id) sender;
- (void) downVote:(id) sender;
- (void) voteFromCell: (UITableViewCell *) cell inDirection:(NSString *) direction;
- (void) globalViewReload;

@end
