//
//  FBLiveViewController.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import "FBLiveViewController.h"
#import "FBDataManager.h"
#import "FBAppDelegate.h"
#import "FBConstants.h"

@interface FBLiveViewController ()

@end

@implementation FBLiveViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Retrieving the application delegate main object.
    mainAppObject = (FBAppDelegate *)[[UIApplication sharedApplication] delegate];
    // Getting the teams names.
    self.homeTeam.text = [[mainAppObject.manager getMatchById:GAME_FOLLOWED] objectForKey:TEAM_A];
    self.awayTeam.text = [[mainAppObject.manager getMatchById:GAME_FOLLOWED] objectForKey:TEAM_B];
	// Fetching the messages from server.
    [self getMessageList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.textInput resignFirstResponder];
    }
}
// Button controller allowing to send a message.
- (IBAction)sendMessage:(id)sender {
    if (self.textInput.text.length > 0 && self.textInput.text.length<=60) {
        // Call the post method and clear the input field if it has been posted.
        if ([mainAppObject.manager postMessageForId:GAME_FOLLOWED text:self.textInput.text]) {
            self.textInput.text = nil;
            [self globalViewReload];
            [self.textInput resignFirstResponder];
        }
    }
}

// Specification of the table.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Returns 0 if the table is void, its size otherwise.
    return (messageList == nil) ? 0 : [messageList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *messageTableIdentifier = @"MessageTableDisplayer";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:messageTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:messageTableIdentifier];
        
        // Standard cell styling.
        UIColor *cellBackground = [UIColor colorWithRed:1
                                                  green:1
                                                   blue:1
                                                  alpha:0.2];
        cell.contentView.backgroundColor = cellBackground;
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.preferredMaxLayoutWidth = 100;
        cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        NSLog(@"max width %f", cell.textLabel.preferredMaxLayoutWidth);

        // Add the up button to a cell.
        UIButton *upButton = [UIButton  buttonWithType:UIButtonTypeCustom];
        upButton.frame = CGRectMake(209, 5, 32, 32);
        [upButton setTitle:@"+" forState:UIControlStateNormal];
        upButton.titleLabel.font = [UIFont systemFontOfSize:22];
        [upButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [upButton addTarget:self action:@selector(upVote:)forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:upButton];
        // Add the down button to a cell.
        UIButton *downButton = [UIButton  buttonWithType:UIButtonTypeCustom];
        downButton.frame = CGRectMake(244, 5, 32, 32);
        [downButton setTitle:@"-" forState:UIControlStateNormal];
        downButton.titleLabel.font = [UIFont systemFontOfSize:22];
        [downButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [downButton addTarget:self action:@selector(downVote:)forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:downButton];
}
    // Add of a label containing the message id.
    cell.textLabel.text = [[messageList objectAtIndex:indexPath.row] objectForKey:@"text"];
    cell.textLabel.tag = [[[messageList objectAtIndex:indexPath.row] objectForKey:@"id"] integerValue];
    NSLog(@"Colors are; %@, %@, %@, %@", cell.backgroundColor, cell.contentView.backgroundColor, cell.textLabel.backgroundColor, cell.detailTextLabel.backgroundColor);
    
    return cell;
}

// Messages management.
- (void) getMessageList
{
    if (mainAppObject.manager.fetched) {
        // If the data has been properly read.
        if (messageList == nil) {
            messageList = [[NSMutableArray alloc]init];
        }
        else {
            [messageList removeAllObjects];
        }
        messageList = [mainAppObject.manager getMessagesById:GAME_FOLLOWED];
    }
}

// Vote triggers. The sender call implies that certain pattern; no argument can be added.
- (void) upVote:(id) sender {
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    [self voteFromCell:clickedCell inDirection:@"up"];
}
- (void) downVote:(id) sender {
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    [self voteFromCell:clickedCell inDirection:@"down"];
}

- (void) voteFromCell: (UITableViewCell *) cell inDirection:(NSString *) direction
{
    NSInteger idMessage = cell.textLabel.tag;
    // The view refresh is triggered only if the vote is a success.
    if ([mainAppObject.manager voteInDirection:direction forMessage:idMessage]) {
        [self globalViewReload];
        // TODO: look for why there is a small latency.
    }
}

- (void) globalViewReload
{
    // Trigger a fetch of the messages, then an update of the message table, and of the table view.
    [mainAppObject.manager resetTimer];
    [self getMessageList];
    [self.messageTable reloadData];
}


@end

