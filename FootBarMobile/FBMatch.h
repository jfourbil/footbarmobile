//
//  FBMatch.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBMatch : NSObject
@property (nonatomic) NSString *teamA;
@property (nonatomic) NSString *teamB;
@property (nonatomic) float lat;
@property (nonatomic) float lon;
@property (nonatomic) NSMutableArray *messages;

@end
