//
//  FBViewController.h
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "FBAppDelegate.h"


@interface FBViewController : UIViewController <CLLocationManagerDelegate,MKMapViewDelegate,MKAnnotation>
{
    BOOL doneInitialZoom;
    CLLocationManager *locationManager;
    FBAppDelegate *mainAppObject;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (void) setClosestGame;

@end
