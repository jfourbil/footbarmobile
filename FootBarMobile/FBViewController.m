//
//  FBViewController.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import "FBViewController.h"
#import "FBAnnotation.h"
#import "FBAppDelegate.h"
#import "FBConstants.h"
#import "FBIconAnnotationView.h"

#define MAP_PADDING 1.1

// we'll make sure that our minimum vertical span is about a kilometer
// there are ~111km to a degree of latitude. regionThatFits will take care of
// longitude, which is more complicated, anyway.
#define MINIMUM_VISIBLE_LATITUDE 0.01
@interface FBViewController ()

@end

@implementation FBViewController
@synthesize mapView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // set delegate in order to manage the annotation views
    mapView.delegate = self;
    // Retrieving the application delegate main object.
    mainAppObject = (FBAppDelegate *)[[UIApplication sharedApplication] delegate];
	// Do any additional setup after loading the view, typically from a nib.
    locationManager = [[CLLocationManager alloc] init];
    if ([CLLocationManager locationServicesEnabled])
    {
        NSLog(@"Location Services Enabled");
        // Switch through the possible location
        // authorization states
        switch([CLLocationManager authorizationStatus]){
            case kCLAuthorizationStatusAuthorized:
                NSLog(@"We have access to location services");
                break;
            case kCLAuthorizationStatusDenied:
                NSLog(@"Location services denied by user");
                break;
            case kCLAuthorizationStatusRestricted:
                NSLog(@"Parental controls restrict location services");
                break;
            case kCLAuthorizationStatusNotDetermined:
                NSLog(@"Unable to determine, possibly not available");
        }
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        // apply current location to the mapView
        CLLocation *location = [locationManager location];
        CLLocationCoordinate2D zoomLocation = [location coordinate];
        NSLog(@"current location : %f - %f",zoomLocation.latitude,zoomLocation.longitude);
        
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 250, 250);
        //        MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
        [mapView setShowsUserLocation:YES];
        [mapView setRegion:viewRegion animated:YES];
        // add matches locations
        [self addMatches:mainAppObject.manager.matches];
        NSLog(@"pin added");
        [self zoomToTheNearestMatch];
        mainAppObject = (FBAppDelegate *)[[UIApplication sharedApplication] delegate];
        //[self setClosestGame];
    } else {
        NSLog(@"Location services are disabled");
    }

}
-(void) zoomToTheNearestMatch
{
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D myCoord = [location coordinate];
    float maxLat=myCoord.latitude, minLat=myCoord.latitude, lat;
    float maxLon=myCoord.longitude, minLon=myCoord.longitude, lon;
    int i;
    for (i=0;i<[mainAppObject.manager.matches count];i++)
    {
        lat =     [[[mainAppObject.manager.matches objectAtIndex:i] objectForKey:LATITUDE] doubleValue];
        lon =     [[[mainAppObject.manager.matches objectAtIndex:i] objectForKey:LONGITUDE] doubleValue];
        NSLog(@"CURRENT : %f - %f",lat, lon);
        minLat = lat <= minLat ? lat : minLat;
        minLon = lon <= minLon ? lon : minLon;
        maxLat = lat >= maxLat ? lat : maxLat;
        maxLon = lon >= maxLon ? lon : maxLon;
    }
    NSLog(@"max : %f - %f",maxLat, maxLon);
    NSLog(@"min : %f - %f",minLat,minLon);

    CLLocationCoordinate2D topLeftCoordinate =
    CLLocationCoordinate2DMake(maxLat,minLon);
    
    MKMapPoint topLeftMapPoint = MKMapPointForCoordinate(topLeftCoordinate);
    
    CLLocationCoordinate2D bottomRightCoordinate =
    CLLocationCoordinate2DMake(minLat, maxLon);
    
    MKMapPoint bottomRightMapPoint = MKMapPointForCoordinate(bottomRightCoordinate);
    
    MKMapRect mapRect = MKMapRectMake(topLeftMapPoint.x,
                                      topLeftMapPoint.y,
                                      fabs(bottomRightMapPoint.x-topLeftMapPoint.x),
                                      fabs(bottomRightMapPoint.y-topLeftMapPoint.y));
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    [mapView setRegion:region animated:YES];
}
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation {
    // Let map view handle user location annotation
    if (annotation == aMapView.userLocation) {
        return nil;    }
    
    // Identifyer for reusing annotationviews
    static NSString *annotationIdentifier = @"icon_annotation";
    
    // Check in queue if there is an annotation view we already can use, else create a new one
    FBIconAnnotationView *annotationView = (FBIconAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!annotationView) {
        annotationView = [[FBIconAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    }
    
    return annotationView;
}
-(void) addMatches:(NSMutableArray *)matches {
    int nbMatches = [matches count];
    int i;
    NSLog(@"%d matches.", nbMatches);
    
    FBAnnotation *annotation;
    NSDictionary *match;
    CLLocationCoordinate2D c;
    NSString * title;
    NSInteger idMatch;
    for(i=0;i<nbMatches;i++){
        match= [matches objectAtIndex:i];
        title = [NSString stringWithFormat:@"%@ - %@", [match objectForKey:TEAM_A], [match objectForKey:TEAM_B]];
        c.latitude =[[match objectForKey:LATITUDE] doubleValue];
        c.longitude = [[match objectForKey:LONGITUDE] doubleValue];
        idMatch = [[match objectForKey:ID_MATCH] doubleValue];
        annotation = [[FBAnnotation alloc] initWithCoordinate:c withName:title withAddress:@"0 - 0" withIdMatch:idMatch];
        [mapView addAnnotation:annotation];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    NSLog(@"clicked");
    FBAnnotation * a = view.annotation;
    GAME_FOLLOWED = a.idMatch;
}
//- (void)locationManager:(CLLocationManager *)manager
//    didUpdateToLocation:(CLLocation *)newLocation
//           fromLocation:(CLLocation *)oldLocation
//{
//    infoLocation.text = [newLocation description];
//}
//
//- (void)locationManager:(CLLocationManager *)manager
//       didFailWithError:(NSError *)error
//{
//    infoLocation.text = [error description];
//}

- (void) setClosestGame
{
    CGFloat lat;
    CGFloat lon;
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D zoomLocation = [location coordinate];
    CGFloat myLat = zoomLocation.latitude;
    CGFloat myLon = zoomLocation.longitude;
    NSLog(@"lat: %f lon: %f", myLat, myLon);
    NSMutableArray * distances = [[NSMutableArray alloc] init];
    int i;
    for (i=0;i<[mainAppObject.manager.matches count];i++)
    {
        lat =     [[[mainAppObject.manager.matches objectAtIndex:i] objectForKey:LATITUDE] doubleValue];
        lon =     [[[mainAppObject.manager.matches objectAtIndex:i] objectForKey:LONGITUDE] doubleValue];
        NSNumber *distance = [NSNumber numberWithFloat:((lat - myLat)*(lat - myLat) + (lon - myLon)*(lon - myLon))];
        NSArray *locationWrapper = [[NSArray alloc] initWithObjects: distance, [[mainAppObject.manager.matches objectAtIndex:i] objectForKey:ID_MATCH], nil];
        [distances addObject:locationWrapper];
    }
    NSLog(@"distances: %@", distances);
    NSLog(@"Closest game: %@",[distances objectAtIndex:0]);
    
}

@end
