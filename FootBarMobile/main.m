//
//  main.m
//  FootBarMobile
//
//  Created by Jérémie FOURBIL on 20/03/13.
//  Copyright (c) 2013 Jérémie FOURBIL Sylvain RACT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FBAppDelegate class]));
    }
}
